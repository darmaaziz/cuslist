package com.onebit.cuslist;

/**
 * Created by daz on 09/07/15.
 */
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

class StudentAdapter extends ArrayAdapter<Student> {

    List<Student> listStudent;
    Context context;
    int layout;

    public StudentAdapter(Context context, int layout, List<Student> listStudent) {
        super(context, layout, listStudent);
        this.context=context;
        this.layout=layout;
        this.listStudent=listStudent;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v=convertView;
        StudentHolder holder;

        if(v==null){
            LayoutInflater vi=((Activity)context).getLayoutInflater();
            v=vi.inflate(layout, parent,false);

            holder=new StudentHolder();
            holder.imageView=(ImageView) v.findViewById(R.id.image);
            holder.nameView=(TextView) v.findViewById(R.id.name);
            holder.majoringView=(TextView) v.findViewById(R.id.majoring);
            holder.ageView=(TextView) v.findViewById(R.id.age);
            holder.sexView=(TextView) v.findViewById(R.id.sex);

            v.setTag(holder);
        }
        else{
            holder=(StudentHolder) v.getTag();
        }

        Student student=listStudent.get(position);
        holder.imageView.setImageResource(student.getImage());
        holder.nameView.setText(student.getName());
        holder.majoringView.setText(student.getMajoring());
        holder.ageView.setText(String.valueOf(student.getAge()));
        holder.sexView.setText(student.getSex());

        return v;
    }

    static class StudentHolder{
        ImageView imageView;
        TextView nameView;
        TextView majoringView;
        TextView ageView;
        TextView sexView;
    }

}
package com.onebit.cuslist;

/**
 * Created by daz on 09/07/15.
 */
public class Student {
    private int image;
    private String name;
    private String majoring;
    private int age;
    private String sex;
    public int getImage() {
        return image;
    }
    public void setImage(int image) {
        this.image = image;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getMajoring() {
        return majoring;
    }
    public void setMajoring(String majoring) {
        this.majoring = majoring;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
}

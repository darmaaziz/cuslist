package com.onebit.cuslist;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView=(ListView) findViewById(R.id.listView);
        List<Student> listStudent=new ArrayList<Student>();

        Student student,student2,student3;

        student=new Student();
        student.setImage(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        student.setName("Agung Setiawan");
        student.setMajoring("Electrical");
        student.setAge(23);
        student.setSex("male");

        listStudent.add(student);

        student2=new Student();
        student2.setImage(R.drawable.abc_ab_share_pack_mtrl_alpha);
        student2.setName("Hauril Maulida Nisfari");
        student2.setMajoring("Electrical");
        student2.setAge(21);
        student2.setSex("female");

        listStudent.add(student2);

        student3=new Student();
        student3.setImage(R.drawable.abc_btn_check_to_on_mtrl_015);
        student3.setName("Akhtar");
        student3.setMajoring("Aero Space");
        student3.setAge(22);
        student3.setSex("male");

        listStudent.add(student3);

        listView.setAdapter(new StudentAdapter(this,R.layout.list_item,listStudent));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}